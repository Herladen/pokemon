passport-mongo
==============

This repository serves as an example of a basic Node.js application which is using [Passport](http://passportjs.org/) as the authentication middleware for authenticating against a locally configured Mongo backend

Steps to run the app
=====================
* After cloning the repo, install the dependencies by running **npm install**
* To start the server, run **npm start** on the base directory

Perquisites
============
The server assumes that you have a local mongo instance running. This means if you have mongo installed locally, all you need to do is configure the db.js file correctly and run the mongod daemon


Pour faire fonctionner le projet
================================
Projet node.js
pour lancer le projet : installer nodejs

* lancer l'invite de commande nodejs dans le dossier du projet
* exécuter la commande npm install && npm start
* une foi démarré le site sera disponible a l'adresse : http://localhost:3000

Base de donnée mongo

* télécharger l'application a cette adresse : http://downloads.mongodb.org/win32/mongodb-win32-i386-v3.0-latest.zip
* déziper l'archive
* lancer un invite de commande depuis le dossier mongo
* executer la commande : mongod --dbpath CHEMIN_DE_L_APPLICATION --storageEngine "mmapv1"
  * par exemple : mongod --dbpath C:\Users\Commodore\Documents\pokemon\data --storageEngine "mmapv1"
* puis depuis un autre invite de commande depuis le dossier mongo executer la commande : mongo
* Si la db a mal été arretté et refuse de démarrer il est possible de la réparer avec la commande : mongod --dbpath CHEMIN_DE_L_APPLICATION\data --repair --storageEngine "mmapv1"
  * ex : mongod --dbpath C:\Users\Commodore\Documents\pokemon\data --repair --storageEngine "mmapv1"