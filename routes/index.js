var express = require('express');
var router = express.Router();
var Pokedex = require('pokedex-promise-v2');
var P = new Pokedex();
var listPokemonJ = require("../json/pokedex.json");




var isAuthenticated = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated())
		return next();
	// if the user is not authenticated then redirect him to the login page
	res.redirect('/');
}

module.exports = function(passport){

	/* GET login page. */
	router.get('/', function(req, res) {
    	// Display the Login page with any flash message, if any
		res.render('index', { message: req.flash('message') });
	});

	/* Handle Login POST */
	router.post('/login', passport.authenticate('login', {
		successRedirect: '/home',
		failureRedirect: '/',
		failureFlash : true  
	}));

	/* GET Registration Page */
	router.get('/signup', function(req, res){
		res.render('register',{message: req.flash('message')});
	});

	/* Handle Registration POST */
	router.post('/signup', passport.authenticate('signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash : true  
	}));

//	/* GET Home Page */
//	router.get('/home', isAuthenticated, function(req, res){
//		res.render('home', { user: req.user });
//	});

	/* Handle Logout */
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	/* GET pokemon Page */
	router.get('/pokemon', isAuthenticated, function(req, res){
        getPokemonList(req, res);
		//res.send('Hello World!');
	});
    /* GET pokemon Page */
//    router.get('/pokemon/:nom', isAuthenticated, function(req, res){
//        P.getPokemonByName(req.params.nom)
//            .then(function(response) {
//
//                res.render('showPokemon', {
//                    Pokemon: response.results
//                });
//
//            })
//            .catch(function(error) {
//                console.log('There was an ERROR: ', error);
//            });
//    });

	/* GET home Page */
	router.get('/home', isAuthenticated, function(req, res) {
        P.getItemsList()
            .then(function(response) {
                res.render('home', {listpokemon: listPokemonJ, listItems: JSON.stringify(response.results)});
				console.log(response.results[5].name);
                //res.render('pokemon', {listPokemon: response.results});
            });
	});

//    function getPokemonList(req, res) {
//        P.getPokemonsList()
//            .then(function(response) {
//                console.log("recup list pokemon")
//                res.render('pokemon', {
//                    listPokemon: response.results
//                });
//            });
//    }

	return router;
}





